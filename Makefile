setup:
	mkdir experiment

remove-all: experiment
	rm -rf experiment

test-add: experiment
	touch experiment/hoge.py
	echo "import __hello__" > experiment/hoge.py
	cargo run init
	cargo run add

test-show-index: experiment/.repo/index
	hexdump -C experiment/.repo/index | less

test-all:
	make setup
	make test-add
	make test-show-index
	make remove-all
