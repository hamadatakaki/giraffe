use std::path::*;
use std::env;

pub fn project_parent_buf() -> std::io::Result<PathBuf> {
    let cwd = env::current_dir()?;
    let parent = cwd.as_path().parent().unwrap();
    Ok(parent.to_path_buf())
}