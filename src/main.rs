extern crate giraffe;
use crate::giraffe::command;

use std::env;

fn main() -> Result<(), std::io::Error> {
    let args: Vec<String> = env::args().collect();

    if let Some(cmd) = args.get(1) {
        let cmd_str = cmd.as_str();
        match cmd_str {
            "init" => command::init()?,
            "add" => command::first_add()?,
            _ => println!("not exist command"),
        }
    } else {
        println!("Please input command")
    }

    Ok(())
 }
