use libflate::zlib;
use std::io::*;

pub fn decompress_zlib(compressed: Vec<u8>) -> std::io::Result<Vec<u8>> {
    let mut decoder = zlib::Decoder::new(&compressed[..])?;
    let mut decompressed = Vec::new();
    decoder.read_to_end(&mut decompressed)?;

    Ok(decompressed)
}

pub fn compress_zlib(vec: Vec<u8>) -> std::io::Result<Vec<u8>> {
    let mut encoder = zlib::Encoder::new(Vec::new()).unwrap();
    encoder.write_all(vec.as_slice())?;
    encoder.finish().into_result()
}
